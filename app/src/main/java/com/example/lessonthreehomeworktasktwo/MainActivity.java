package com.example.lessonthreehomeworktasktwo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText decBin;
    EditText decOct;
    EditText decHex;
    EditText bin;
    EditText oct;
    EditText hex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        decBin = findViewById(R.id.dec_bin);
        decOct = findViewById(R.id.dec_oct);
        decHex = findViewById(R.id.dec_hex);
        bin = findViewById(R.id.bin);
        oct = findViewById(R.id.oct);
        hex = findViewById(R.id.hex);

        final TextWatcher twBin = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //do nothing
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!decBin.getText().toString().isEmpty()) {
                    int b = Integer.parseInt(decBin.getText().toString());
                    bin.setText(Integer.toBinaryString(b));
                } else {
                    bin.setText("");
                }
            }
        };

        final TextWatcher twOct = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //do nothing
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!decOct.getText().toString().isEmpty()) {
                    int o = Integer.parseInt(decOct.getText().toString());
                    oct.setText(Integer.toOctalString(o));
                } else {
                    oct.setText("");
                }
            }
        };

        final TextWatcher twHex = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //do nothing
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!decHex.getText().toString().isEmpty()) {
                    int h = Integer.parseInt(decHex.getText().toString());
                    hex.setText(Integer.toHexString(h));
                } else {
                    hex.setText("");
                }
            }
        };

        decBin.addTextChangedListener(twBin);
        decOct.addTextChangedListener(twOct);
        decHex.addTextChangedListener(twHex);
    }
}
